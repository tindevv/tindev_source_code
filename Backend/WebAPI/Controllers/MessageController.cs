﻿using AutoMapper;
using Business.Abstract;
using Business.Helpers;
using Core.Utilities.Helpers;
using Entities.Concrete;
using Entities.DTOs;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        private readonly IMessageService _messageService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        public MessageController(IMessageService messageService, IMapper mapper, IUserService userService)
        {
            _mapper = mapper;
            _messageService = messageService;
            _userService = userService;
        }

        [HttpGet("{id}", Name = "GetMessageById")]
        public async Task<IActionResult> GetMessage(int userId, int id)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var messageFromRepo = await _messageService.GetMessageById(id);

            if (messageFromRepo == null)
                return NotFound();

            return Ok(messageFromRepo);
        }

        [HttpGet]
        public async Task<IActionResult> GetMessagesForUser(int userId, [FromQuery] MessageParams messageParams)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            messageParams.UserId = userId;

            var messagesFromRepo = await _messageService.GetMessagesForUser(messageParams);

            var messages = _mapper.Map<IEnumerable<MessageToReturnDto>>(messagesFromRepo);

            return Ok(messages);
        }

        [HttpGet("thread/{recipientId}")]
        public async Task<IActionResult> GetMessageThread(int userId, int recipientId)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var messageFromRepo = await _messageService.GetMessageThread(userId, recipientId);

            var messageThread = _mapper.Map<IEnumerable<MessageToReturnDto>>(messageFromRepo);

            return Ok(messageThread);
        }

        [HttpPost("createmessage/{userId}")]
        public async Task<IActionResult> CreateMessage(int userId, MessageForCreationDto messageForCreationDto)
        {

            try
            {
                messageForCreationDto.SenderId = userId;

                var recipient = await _userService.GetUser(messageForCreationDto.RecipientId);

                if (recipient == null)
                    return BadRequest("User not found!");

                var message = _mapper.Map<Message>(messageForCreationDto);

                _messageService.AddForMessage(message);

                var messageToReturn = _mapper.Map<MessageToReturnDto>(message);
                if (await _messageService.SaveAll())
                {
                    

                    return CreatedAtRoute("GetMessageById", new { id = message.Id }, messageToReturn);
                }

                throw new Exception("Failed on saving new message");

            }catch(Exception e)
            {
                throw new Exception(e.Message);
            }


            }

        [HttpPost("{id}")]
        public async Task<IActionResult> DeleteMessage(int id, int userId)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var messageFromRepo = await _messageService.GetMessageById(id);

            if (messageFromRepo.SenderId == userId)
                messageFromRepo.SenderDeleted = true;

            if (messageFromRepo.RecipientId == userId)
                messageFromRepo.RecipientDeleted = true;

            if (messageFromRepo.SenderDeleted && messageFromRepo.RecipientDeleted)
                _messageService.Delete(messageFromRepo);

            if (await _messageService.SaveAll())
                return NoContent();

            throw new Exception("Error Deleting the message");
        }

        [HttpPost("{id}/read")]
        public async Task<IActionResult> MarkMessageAsRead(int userId, int id)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var message = await _messageService.GetMessageById(id);

            if (message.RecipientId != userId)
                return Unauthorized();

            message.IsRead = true;
            message.DateRead = DateTime.Now;

            await _messageService.SaveAll();

            return NoContent();
        }
    }
}
