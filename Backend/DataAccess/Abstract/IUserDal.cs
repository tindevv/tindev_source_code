﻿using Core.DataAccess;
using Core.Entities.Concrete;
using Core.Utilities.Helpers;
using Entities.Concrete;
using Entities.DTOs;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DataAccess.Abstract
{
    public interface IUserDal : IEntityRepository<User>
    {
        Task<User> GetUser(int id);
        Task<PagedList<User>> GetUsers(UserParams userParams);
        List<OperationClaim> GetClaims(User user);
        List<UserDto> GetUsersDtos(Expression<Func<UserDto, bool>> filter = null);
        Task<IEnumerable<int>> GetUserLikes(int id, bool likers);
        Task<bool> SaveAll();
        
    }
}
