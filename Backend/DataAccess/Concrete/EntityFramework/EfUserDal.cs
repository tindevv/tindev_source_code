﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Core.DataAccess.EntityFramework;
using Core.Entities.Concrete;
using Core.Utilities.Helpers;
using DataAccess.Abstract;
using Entities.Concrete;
using Entities.DTOs;
using Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfUserDal : EfEntityRepositoryBase<User, TindevContext>, IUserDal
    {
        public List<OperationClaim> GetClaims(User user)
        {
            using (var context = new TindevContext())
            {
                var result = from operationClaim in context.operationclaims
                             join userOperationClaim in context.useroperationclaims
                                 on operationClaim.Id equals userOperationClaim.OperationClaimId
                             where userOperationClaim.UserId == user.UserID
                             select new OperationClaim { Id = operationClaim.Id, Name = operationClaim.Name };
                return result.ToList();
            }
        }

        public List<UserDto> GetUsersDtos(Expression<Func<UserDto, bool>> filter = null)
        {
            using (var context = new TindevContext())
            {
                var result = from user in context.users
                             select new UserDto
                             {
                                 Id = user.UserID,
                                 Email = user.Email,
                                 FirstName = user.FirstName,
                                 LastName = user.LastName
                             };
                return filter == null
                    ? result.ToList()
                    : result.Where(filter).ToList();
            }
        }

        public async Task<bool> SaveAll()
        {
            using (var context = new TindevContext())
            {
                return await context.SaveChangesAsync() > 0;
            }
        }

        public async Task<IEnumerable<int>> GetUserLikes(int id, bool likers)
        {
            using(var context = new TindevContext())
            {
                var user = await context.users
                .Include(x => x.Likers)
                .Include(x => x.Likees)
                .FirstOrDefaultAsync(u => u.UserID == id);

                if (likers)
                {
                    return user.Likers.Where(u => u.LikeeId == id).Select(i => i.LikerId).ToList();
                }
                else
                {
                    return user.Likees.Where(u => u.LikerId == id).Select(i => i.LikeeId).ToList();
                }

            }
        }

        public async Task<User> GetUser(int id)
        {
            using (var context = new TindevContext())
            {
                var user = await context.users.Include(p => p.UserImages).FirstOrDefaultAsync(u => u.UserID == id);

                return user;
            }
        }

        public async Task<PagedList<User>> GetUsers(UserParams userParams)
        {
            using (var context = new TindevContext())
            {
                var users = context.users.Include(p => p.UserImages).OrderByDescending(u => u.LastActive).AsQueryable();

                users = users.Where(u => u.UserID != userParams.UserId);

                users = users.Where(u => u.Gender == userParams.Gender);

                if (userParams.Likers)
                {
                    var userLikers = await GetUserLikes(userParams.UserId, userParams.Likers);

                    users = users.Where(u => userLikers.Contains(u.UserID));
                }

                if (userParams.Likees)
                {
                    var userLikees = await GetUserLikes(userParams.UserId, userParams.Likers);

                    users = users.Where(u => userLikees.Contains(u.UserID));
                }

                if (userParams.MinAge != 18 || userParams.MaxAge != 99)
                {
                    var minDob = DateTime.Today.AddYears(-userParams.MaxAge - 1);
                    var maxDob = DateTime.Today.AddYears(-userParams.MinAge);

                    users = users.Where(u => u.DateOfBirth >= minDob && u.DateOfBirth <= maxDob);
                }

                if (!string.IsNullOrEmpty(userParams.OrderBy))
                {
                    switch (userParams.OrderBy)
                    {
                        case "created":
                            users = users.OrderByDescending(u => u.Created);
                            break;
                        default:
                            users = users.OrderByDescending(u => u.LastActive);
                            break;
                    }
                }

                return await PagedList<User>.CreateAsync(users, userParams.PageNumber, userParams.PageSize);
            }

        }


    }
}
