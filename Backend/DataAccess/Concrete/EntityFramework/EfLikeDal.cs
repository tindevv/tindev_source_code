﻿using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfLikeDal : EfEntityRepositoryBase<Like, TindevContext>, ILikeDal
    {
        

        public async Task<Like> GetLike(int userId, int recipientId)
        {
            using (var context = new TindevContext())
            {
                return await context.likes.FirstOrDefaultAsync(u => u.LikerId == userId && u.LikeeId == recipientId);
            }
        }

        public async Task<bool> SaveAll()
        {
            using (var context = new TindevContext())
            {
                return await context.SaveChangesAsync() > 0;
            }
        }
    }
}
