﻿using Core.DataAccess.EntityFramework;
using Core.Utilities.Helpers;
using DataAccess.Abstract;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfMessageDal : EfEntityRepositoryBase<Message, TindevContext>, IMessageDal
    {
        public async Task<Message> GetMessage(int id)
        {
            using (var context = new TindevContext())
            {
                return await context.messages.FirstOrDefaultAsync(m => m.Id == id);
            }
        }

        public async Task<PagedList<Message>> GetMessagesForUser(MessageParams messageParams)
        {
            using (var context = new TindevContext())
            {
                var messages = context.messages
              .Include(u => u.Sender).ThenInclude(i => i.UserImages)
              .Include(u => u.Recipient).ThenInclude(i => i.UserImages)
              .AsQueryable();

                switch (messageParams.MessageContainer)
                {
                    case "Inbox":
                        messages = messages.Where(u => u.RecipientId == messageParams.UserId && u.RecipientDeleted == false);
                        break;
                    case "Outbox":
                        messages = messages.Where(u => u.SenderId == messageParams.UserId && u.SenderDeleted == false);
                        break;
                    default:
                        messages = messages.Where(u => u.RecipientId == messageParams.UserId && u.RecipientDeleted == false
                         && u.IsRead == false);
                        break;
                }

                messages = messages.OrderByDescending(d => d.MessageSent);

                return await PagedList<Message>.CreateAsync(messages, messageParams.PageNumber, messageParams.PageSize);
            }
        }

        public async Task<IEnumerable<Message>> GetMessageThread(int userId, int recipientId)
        {
            using (var context = new TindevContext())
            {
                var messages = await context.messages
                    .Include(u => u.Sender).ThenInclude(i => i.UserImages)
                    .Include(u => u.Recipient).ThenInclude(i => i.UserImages)
                    .Where(m => m.RecipientId == userId && m.RecipientDeleted == false
                        && m.SenderId == recipientId
                        || m.RecipientId == recipientId && m.SenderId == userId
                        && m.SenderDeleted == false)
                    .OrderByDescending(m => m.MessageSent)
                    .ToListAsync();

                return messages;
            }
        }

        public async Task<bool> SaveAll()
        {
            using (var context = new TindevContext())
            {
                return await context.SaveChangesAsync() > 0;
            }
        }
        
        public void AddForMessage(Message entity)
        {
            
            using (var context = new TindevContext())
            {
                var addedEntity = context.AddAsync(entity);
                context.SaveChanges();
            }
        }
    }
}
