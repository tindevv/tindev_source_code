﻿using Core.Entities.Concrete;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Concrete.EntityFramework
{
    //Contex : Db tabloları ile proje classlarını bağlamak 
    public class TindevContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Database=Tindev;Username=postgres;Password=ilhan123");
        }
        
        public DbSet<User> users { get; set; }
        public DbSet<Channel> channels { get; set; }
        public DbSet<OperationClaim> operationclaims { get; set; }
        public DbSet<UserOperationClaim> useroperationclaims { get; set; }
        public DbSet<Message> messages { get; set; }
        public DbSet<Like> likes { get; set; }
        public DbSet<UserImage> userimages { get; set; }


        //projen hangi veritabanıyla ilişkili onun belirtildiği yer
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(TindevContext).Assembly);

            modelBuilder.Entity<Like>()
                .HasKey(k => new { k.LikerId, k.LikeeId });

            modelBuilder.Entity<Like>()
              .HasOne(d => d.Likee)
              .WithMany(d => d.Likers)
              .HasForeignKey(d => d.LikeeId)
              .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Like>()
              .HasOne(d => d.Liker)
              .WithMany(d => d.Likees)
              .HasForeignKey(d => d.LikerId)
              .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Message>()
              .HasOne(d => d.Sender)
              .WithMany(m => m.MessagesSent)
              .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Message>()
              .HasOne(d => d.Recipient)
              .WithMany(m => m.MessagesReceived)
              .OnDelete(DeleteBehavior.Restrict);

            
        }

       

    }
}
