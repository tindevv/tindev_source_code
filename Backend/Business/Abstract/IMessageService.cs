﻿using Business.Helpers;
using Core.Utilities.Helpers;
using Core.Utilities.Results.Abstract;
using Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface IMessageService
    {
        IResult Add(Message message);
        void AddForMessage(Message message);
        IResult Delete(Message message);
        Task<Message> GetMessageById(int id);
        Task<PagedList<Message>> GetMessagesForUser(MessageParams messageParams);
        Task<IEnumerable<Message>> GetMessageThread(int userId, int recipientId);
        Task<bool> SaveAll();
    }
}
