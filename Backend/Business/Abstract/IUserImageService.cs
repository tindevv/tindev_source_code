﻿using Core.Utilities.Results.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface IUserImageService
    {
        IDataResult<List<UserImage>> GetAll();
        IDataResult<List<UserImage>> GetUserImages(int userId);
        IDataResult<UserImage> GetById(int imageId);
        IResult Add(IFormFile file, int userId);
        IResult Update(UserImage userImage, IFormFile file);
        IResult Delete(UserImage userImage);
        IResult DeleteAllImagesOfUserByUserId(int userId);
        
    }
}
