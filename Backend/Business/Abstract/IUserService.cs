﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities.Concrete;
using Core.Utilities.Helpers;
using Core.Utilities.Results.Abstract;
using Entities.Concrete;
using Entities.DTOs;
using Entities.Models;

namespace Business.Abstract
{
    public interface IUserService
    {
        Task<PagedList<User>> GetUsers(UserParams userParams);
        Task<User> GetUser(int id);  
        IDataResult<List<UserDto>> GetAllDto();      
        IDataResult<UserDto> GetUserDtoById(int userId);
        IResult Add(User user);
        IResult Delete(int userId);
        IResult Update(User user);
        IResult UpdateByDto(UserDto userDto);
        IDataResult<List<OperationClaim>> GetClaims(User user);
        IDataResult<User> GetUserByMail(string email);
        IDataResult<UserDto> GetUserDtoByMail(string email);
        Task<bool> SaveAll();
        Task<IEnumerable<int>> GetUserLikes(int id, bool likers);

    }
}