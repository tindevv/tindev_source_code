﻿using Core.Utilities.Results.Abstract;
using Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Abstract
{
    public interface ILikeService
    {
        IResult Add(Like like);
        Task<Like> GetLike(int userId, int recipientId);
        Task<bool> SaveAll();
    }
}
