﻿using Core.Utilities.Results.Abstract;
using Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Abstract
{
    public interface IChannelService
    {
        IDataResult<Channel> GetChannelById(int channelId);
        IDataResult<List<Channel>> GetAll();
        IResult Add(Channel channel);
        IResult Update(Channel channel);
        IResult Delete(Channel channel);

        
    }
}
