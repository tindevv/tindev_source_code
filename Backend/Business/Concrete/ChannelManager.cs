﻿using Business.Abstract;
using Business.BusinessAspects.Autofac;
using Business.Constants;
using Business.ValidationRules.FluentValidation;
using Core.Aspects.Autofac.Validation;
using Core.Utilities.Business;
using Core.Utilities.Results.Abstract;
using Core.Utilities.Results.Concrete;
using DataAccess.Abstract;
using Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business.Concrete
{
    public class ChannelManager : IChannelService
    {
        private readonly IChannelDal _channelDal;
        public ChannelManager(IChannelDal channelDal)
        {
            _channelDal = channelDal;
        }
        public IDataResult<Channel> GetChannelById(int channelId)
        {
            return new SuccessDataResult<Channel>(_channelDal.Get(c => c.channelID == channelId), Messages.ChannelListed);
        }

        public IDataResult<List<Channel>> GetAll()
        {
            return new SuccessDataResult<List<Channel>>(_channelDal.GetAll(), Messages.ChannelListed);
        }
        [ValidationAspect(typeof(ChannelValidator))]
       // [SecuredOperation("admin")]
        public IResult Add(Channel channel)
        {
            var rulesResult = BusinessRules.Run(/*CheckIfChannelNameExist(channel.channelname)*/);
            if (rulesResult != null)
            {
                return rulesResult;
            }
            _channelDal.Add(channel);
            return new SuccessResult(Messages.ChannelAdded);
        }
        [SecuredOperation("admin")]
        public IResult Delete(Channel channel)
        {
            var rulesResult = BusinessRules.Run(CheckIfChannelIdExist(channel.channelID));
            if (rulesResult != null)
            {
                return rulesResult;
            }
            var deletedChannel = _channelDal.Get(c => c.channelID == channel.channelID);
            _channelDal.Delete(deletedChannel);
            return new SuccessResult(Messages.ChannelDeleted);
        }
        [ValidationAspect(typeof(ChannelValidator))]
        [SecuredOperation("admin")]
        public IResult Update(Channel channel)
        {
            var rulesResult = BusinessRules.Run(CheckIfChannelNameExist(channel.channelname),
                CheckIfChannelIdExist(channel.channelID));
            if (rulesResult != null)
            {
                return rulesResult;
            }
            _channelDal.Update(channel);
            return new SuccessResult(Messages.ChannelUpdated);
        }
        //Ruless
        private IResult CheckIfChannelIdExist(int channelId)
        {
            var result = _channelDal.GetAll(c => Equals(c.channelID, channelId)).Any();
            if (!result)
            {
                return new ErrorResult(Messages.ChannelNotExist);
            }
            return new SuccessResult();
        }

        private IResult CheckIfChannelNameExist(string channelName)
        {
            var result = _channelDal.GetAll(c => Equals(c.channelname, channelName)).Any();
            if (result)
            {
                return new ErrorResult(Messages.ChannelExist);
            }
            return new SuccessResult();
        }
       
    }
}
