﻿using Business.Abstract;
using Business.BusinessAspects.Autofac;
using Business.Constants;
using Business.ValidationRules.FluentValidation;
using Core.Aspects.Autofac.Caching;
using Core.Aspects.Autofac.Validation;
using Core.Utilities.Business;
using Core.Utilities.Helpers;
using Core.Utilities.Results.Abstract;
using Core.Utilities.Results.Concrete;
using DataAccess.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class UserImageManager : IUserImageService
    {
        private readonly IUserImageDal _userImageDal;

        public UserImageManager(IUserImageDal userImageDal)
        {
            _userImageDal = userImageDal;
        }

        [SecuredOperation("admin")]
        public IDataResult<List<UserImage>> GetAll()
        {
            return new SuccessDataResult<List<UserImage>>(_userImageDal.GetAll(), Messages.UserImagesListed);
        }

        [SecuredOperation("admin")]
        public IDataResult<List<UserImage>> GetUserImages(int userId)
        {
            var checkIfUserImage = CheckIfUserHasImage(userId);
            var images = checkIfUserImage.Success
                ? checkIfUserImage.Data
                : _userImageDal.GetAll(u=>u.UserId == userId);
            return new SuccessDataResult<List<UserImage>>(images, checkIfUserImage.Message);
        }

        [SecuredOperation("admin")]
        public IDataResult<UserImage> GetById(int imageId)
        {
            return new SuccessDataResult<UserImage>(_userImageDal.Get(c => c.Id == imageId), Messages.UserImageListed);
        }
        [SecuredOperation("admin")]
        [ValidationAspect(typeof(UserImageValidator))]
        public IResult Add(IFormFile file, int userId)
        {
            IResult rulesResult = BusinessRules.Run(CheckIfUserImageLimitExceeded(userId));
            if (rulesResult != null)
            {
                return rulesResult;
            }

            var imageResult = FileHelper.Upload(file);
            if (!imageResult.Success)
            {
                return new ErrorResult(imageResult.Message);
            }

            UserImage userImage = new UserImage
            {
                ImagePath = imageResult.Message,
                UserId = userId,
                DateAdded = DateTime.Now
            };
            _userImageDal.Add(userImage);
            return new SuccessResult(Messages.UserImageAdded);
        }

        [SecuredOperation("admin")]
        [ValidationAspect(typeof(UserImageValidator))]
        public IResult Update(UserImage userImage, IFormFile file)
        {
            IResult rulesResult = BusinessRules.Run(CheckIfUserImageIdExist(userImage.Id),
                CheckIfUserImageLimitExceeded(userImage.UserId));
            if (rulesResult != null)
            {
                return rulesResult;
            }

            var updatedImage = _userImageDal.Get(u => u.Id == userImage.Id);
            var result = FileHelper.Update(file, updatedImage.ImagePath);
            if (!result.Success)
            {
                return new ErrorResult(Messages.ErrorUpdatingImage);
            }
            userImage.ImagePath = result.Message;
            userImage.DateAdded = DateTime.Now;
            _userImageDal.Update(userImage);
            return new SuccessResult(Messages.UserImageUpdated);
        }

        [SecuredOperation("admin")]
        public IResult Delete(UserImage userImage)
        {
            IResult rulesResult = BusinessRules.Run(CheckIfUserImageIdExist(userImage.Id));
            if (rulesResult != null)
            {
                return rulesResult;
            }

            var deletedImage = _userImageDal.Get(u => u.Id == userImage.Id);
            var result = FileHelper.Delete(deletedImage.ImagePath);
            if (!result.Success)
            {
                return new ErrorResult(Messages.ErrorDeletingImage);
            }
            _userImageDal.Delete(deletedImage);
            return new SuccessResult(Messages.UserImageDeleted);
        }

        [SecuredOperation("admin")]
        public IResult DeleteAllImagesOfUserByUserId(int userId)
        {
            var deletedImages = _userImageDal.GetAll(u => u.UserId == userId);
            if (deletedImages == null)
            {
                return new ErrorResult(Messages.NoPictureOfTheUser);
            }
            foreach (var deletedImage in deletedImages)
            {
                _userImageDal.Delete(deletedImage);
                FileHelper.Delete(deletedImage.ImagePath);
            }
            return new SuccessResult(Messages.UserImageDeleted);
        }

        
        


        //Business Rules

        private IResult CheckIfUserImageLimitExceeded(int userId)
        {
            int result = _userImageDal.GetAll(u=>u.UserId == userId).Count;
            if (result >= 5)
            {
                return new ErrorResult(Messages.UserImageLimitExceeded);
            }
            return new SuccessResult();
        }

        private IDataResult<List<UserImage>> CheckIfUserHasImage(int userId)
        {
            string logoPath = "/images/default.jpg";
            bool result = _userImageDal.GetAll(u => u.UserId == userId).Any();
            if (!result)
            {
                List<UserImage> imageList = new List<UserImage>
                {
                    new UserImage
                    {
                        ImagePath = logoPath,
                        UserId = userId,
                        DateAdded = DateTime.Now
                    }
                };
                return new SuccessDataResult<List<UserImage>>(imageList, Messages.GetDefaultImage);
            }
            return new ErrorDataResult<List<UserImage>>(new List<UserImage>(), Messages.UserImagesListed);
        }

        private IResult CheckIfUserImageIdExist(int imageId)
        {
            var result = _userImageDal.GetAll(u => u.Id == imageId).Any();
            if (!result)
            {
                return new ErrorResult(Messages.UserImageIdNotExist);
            }
            return new SuccessResult();
        }

        
    }
}
