﻿using Business.Abstract;
using Core.Utilities.Results.Abstract;
using Core.Utilities.Results.Concrete;
using DataAccess.Abstract;
using Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class LikeManager : ILikeService
    {
        private readonly ILikeDal _likeDal;

        public LikeManager(ILikeDal likeDal)
        {
            _likeDal = likeDal;
        }

        public IResult Add(Like like)
        {
            _likeDal.Add(like);
            return new SuccessResult();
        }

        public async Task<Like> GetLike(int userId, int recipientId)
        {
            var result = await _likeDal.GetLike(userId, recipientId);
            return result;  
        }
        public async Task<bool> SaveAll()
        {
            var result = await _likeDal.SaveAll();
            return result;
        }
    }
}
