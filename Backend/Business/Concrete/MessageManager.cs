﻿using Business.Abstract;
using Business.Constants;
using Business.Helpers;
using Core.Utilities.Business;
using Core.Utilities.Helpers;
using Core.Utilities.Results.Abstract;
using Core.Utilities.Results.Concrete;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class MessageManager : IMessageService
    {
        private readonly IMessageDal _messageDal;
        private readonly IUserService _userService;
        public MessageManager(IMessageDal messageDal, IUserService userService)
        {
            _messageDal = messageDal;
            _userService = userService;
        }

        public IResult Add(Message message)
        {
            var rulesResult = BusinessRules.Run();
            if (rulesResult != null)
            {
                return rulesResult;
            }
            _messageDal.Add(message);
            return new SuccessResult(Messages.MessageAdded);
        }
        public void AddForMessage(Message message)
        {
            _messageDal.AddForMessage(message);
        }

        public IResult Delete(Message message)
        {
            var rulesResult = BusinessRules.Run();
            if (rulesResult != null)
            {
                return rulesResult;
            }
            var deletedMessage = _messageDal.Get(m=>m.Id == message.Id);
            _messageDal.Delete(deletedMessage);
            return new SuccessResult(Messages.MessageDeleted);
        }

        public async Task<Message> GetMessageById(int id)
        {
            var result = await (_messageDal.GetMessage(id));
            return result;
        }

        public async Task<PagedList<Message>> GetMessagesForUser(MessageParams messageParams)
        {
            var result = await (_messageDal.GetMessagesForUser(messageParams));
            return result;

        }

        public async Task<IEnumerable<Message>> GetMessageThread(int userId, int recipientId)
        {
            var result = await (_messageDal.GetMessageThread(userId, recipientId));
            return result;
        }
        public async Task<bool> SaveAll()
        {
            var result = await _messageDal.SaveAll();
            return result;
        }
        


    }
}
