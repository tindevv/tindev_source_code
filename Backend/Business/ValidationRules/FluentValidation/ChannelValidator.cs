﻿using Entities.Concrete;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.ValidationRules.FluentValidation
{
    public class ChannelValidator : AbstractValidator<Channel>
    {
        public ChannelValidator()
        {
            RuleFor(c => c.channelname).NotEmpty();
            RuleFor(c => c.channelname).NotNull();
            RuleFor(c => c.channelname).MaximumLength(50);
            RuleFor(c => c.channelname).MinimumLength(2);


        }
    }
}
