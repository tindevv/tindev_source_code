﻿using Core.Entities.Concrete;
using Entities.Concrete;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Business.ValidationRules.FluentValidation
{
    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(u => u.FirstName).NotEmpty();
            RuleFor(u=>u.FirstName).MinimumLength(2);
            RuleFor(u => u.LastName).NotEmpty();
            RuleFor(u => u.LastName).MinimumLength(2);
            //RuleFor(u=>u.ExperienceYear).GreaterThanOrEqualTo(2).When();
            //RuleFor(u => u.Email).Must(CheckRegexForEmail);
            //RuleFor(u => u.Password).Must(CheckRegexForPassword);
        }

        private bool CheckRegexForEmail(string email)
        {
            string regexForEmail = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+.(com|org|net|edu|gov|mil|biz|info|mobi)(.[A-Z]{2})?$";
            Regex reStrict = new Regex(regexForEmail);
            bool isStrictMatch = reStrict.IsMatch(email);
            return isStrictMatch;
        }

        private bool IsNickNameValid(string nickName)
        {
            Regex regex = new Regex(@"^(?=[a-zA-Z])[-\w.]{5,16}([a-zA-Z\d]|(?<![-.])_)$");
            return regex.IsMatch(nickName);
        }
    }
}
