﻿using Entities.DTOs;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Business.ValidationRules.FluentValidation
{
    public class UserForLoginDtoValidator : AbstractValidator<UserForLoginDto>
    {
        public UserForLoginDtoValidator()
        {
            RuleFor(u => u.Email).Must(CheckRegexForEmail);
            RuleFor(u => u.Password).Must(CheckRegexForPassword);
        }
        private bool CheckRegexForEmail(string email)
        {
            string regexForEmail = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+.(com|org|net|edu|gov|mil|biz|info|mobi)(.[A-Z]{2})?$";
            Regex reStrict = new Regex(regexForEmail);
            bool isStrictMatch = reStrict.IsMatch(email);
            return isStrictMatch;
        }

        private bool CheckRegexForPassword(string password)
        {
            Regex regex = new Regex("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@!%*?+#&'()[=\"€])[A-Za-z\\d$@!%*?+#&'()[=\"€']{8,}");
            return regex.IsMatch(password);
        }
    }
}
