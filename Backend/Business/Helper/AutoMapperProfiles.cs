﻿using AutoMapper;
using Core.Extensions;
using Entities.Concrete;
using Entities.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<User, UserForDetailDto>()
                .ForMember(dest => dest.ImagePath, opt =>
                {
                    opt.MapFrom(src => src.UserImages.FirstOrDefault(p => p.IsMain).ImagePath);
                })
                .ForMember(dest => dest.Age, opt =>
                {
                    opt.MapFrom(d => d.DateOfBirth.CalculateAge());
                });
            CreateMap<User, UserForDetailDto>().ForMember(dest => dest.ImagePath, opt =>
            {
                opt.MapFrom(src => src.UserImages.FirstOrDefault(p => p.IsMain).ImagePath);
            }).ForMember(dest => dest.Age, opt =>
            {
                opt.MapFrom(d => d.DateOfBirth.CalculateAge());
            });
            CreateMap<UserImage, ImagesForDetailedDto>();
            CreateMap<UserForUpdateDto, User>();
            CreateMap<UserImage, ImageForReturnDto>();
            CreateMap<ImageForCreationDto, UserImage>();
            CreateMap<UserForRegisterDto, User>();
            CreateMap<MessageForCreationDto, Message>().ReverseMap();
            CreateMap<Message, MessageToReturnDto>()
                    .ForMember(m => m.SenderPhotoUrl,
                               opt => opt.MapFrom(u => u.Sender.UserImages.FirstOrDefault(p => p.IsMain).ImagePath))
                    .ForMember(m => m.RecipientPhotoUrl,
                               opt => opt.MapFrom(u => u.Recipient.UserImages.FirstOrDefault(p => p.IsMain).ImagePath));

        }
    }
}
