﻿using Core.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Business.Constants
{
    public static class Messages
    {
        public static string NickNameAlreadyExists = "Nickname daha önce kullanılmış";
        public static object NotHaveUsersinCity;
        public static string UsersImagesListed;
        public static string GetDefaultImage;
        public static string UserImagesListed;
        public static string UserImageLimitExceeded;



        public static string AuthorizationDenied = "Bu islemi yapmak icin yetkiniz yok";
        public static string UserRegistered = "Kullanici kayit basarili";
        public static string UserNotFound = "Kullanici bulunamadi";
        public static string PasswordError = "Sifre hatali";
        public static string SuccessfulLogin = "Giris basarili";
        public static string UserAlreadyExists = "Kullanici zaten sisteme kayitli";
        public static string AccessTokenCreated = "Token basariyla olusturuldu";
        public static string PasswordChanged = "Sifre basariyla degistirildi";

        public static string UserAdded = "Kullanici eklendi";
        public static string UserDeleted = "Kullanici silindi";
        public static string UserUpdated = "Kullanici guncellendi";
        public static string UsersListed = "Kullanicilar listelendi";
        public static string UserListed = "Kullanici listelendi";
        public static string UserNotExist = "Kullanici mevcut degil";
        public static string UserEmailExist = "E-mail zaten kayitli";
        public static string UserEmailNotAvailable = "Kullanici e-maili gecersiz";
        public static string UserImageIdNotExist = "";
        public static string UserImageListed;
        public static string UserImageAdded;
        public static string ErrorUpdatingImage;
        public static string UserImageUpdated;
        public static string ErrorDeletingImage;
        public static string UserImageDeleted;
        public static string NoPictureOfTheUser;
        public static string ChannelExist;
        public static string ChannelNotExist;
        public static string ChannelListed;
        public static string ChannelAdded;
        public static string ChannelUpdated;
        public static string ChannelDeleted;
        public static string MessagesListed;
        public static string MessageAdded;
        public static string MessageDeleted;
        public static string MessageListed;
    }
}
