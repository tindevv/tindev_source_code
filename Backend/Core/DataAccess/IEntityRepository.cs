﻿using Core.Entities;
using Core.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Core.DataAccess
{
    // T'yi sınırlandırıyorum(Generic Constraint)
    // class:ref tip olmalı
    // new(): interfaceler newlenemez bu yüzden IEntity yazamam T yerine
    public interface IEntityRepository<T> where T:class,IEntity,new()
    {
        //Expression filtre yazabilmemi sağlayan delege
        List<T> GetAll(Expression<Func<T,bool>> filter=null);
        T Get(Expression<Func<T, bool>> filter);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
