﻿using Core.Entities.Abstract;
using System;

namespace Entities.DTOs
{
    public class UserForRegisterDto : IDto
    {
        public UserForRegisterDto()
        {
            Created = DateTime.Now;
            LastActive = DateTime.Now;
        }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Status { get; set; }
        public string NickName { get; set; }
        public int ChannelId { get; set; }
        public int ExperienceYear { get; set; }
        public string Description { get; set; }
        public string Gender { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastActive { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        
    }
}