﻿using Core.Entities.Abstract;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.DTOs
{
    public class ImageForCreationDto : IDto
    {
        public ImageForCreationDto()
        {
            DateAdded = DateTime.Now;
        }
        public string ImagePath { get; set; }

        public IFormFile File { get; set; }
        public string Description { get; set; }

        public DateTime DateAdded { get; set; }

        public string PublicId { get; set; }


    }
}