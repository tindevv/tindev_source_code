﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Helpers
{
    public static class PageExtensions
    {
        public static int CalculateAge(this DateTime theDateTime)
        {
            var age = DateTime.Today.Year - theDateTime.Year;
            if (theDateTime.AddYears(age) > DateTime.Today)
                age--;

            return age;
        }
    }
}
